package com.example.qbw.imagecache;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.qbw.imagecache.ImageCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {

    private ImageCache imageCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //构造imageCache对象
        String cacheDirName = "jly_imageCache";
        int cacheSize = 1 * 1024 * 1024;
        imageCache = new ImageCache.Builder().cacheName(cacheDirName).cacheSize(cacheSize).build(this);
        test();
    }

    private void test() {
        new Thread() {
            @Override
            public void run() {
                testLocal();
                testNet();
            }
        }.start();
    }

    private void testLocal() {

        String temUrl = "http:/xxxx.com/yyy.png";
        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "a1.jpg");
            for (int i = 0; i < 10; i++) {
                String key = temUrl + i;
                imageCache.addCache(key, new FileInputStream(file));
                Log.d("cache", "添加缓存:" + key);
            }

            for (int i = 0; i < 10; i++) {
                String key = temUrl + i;
                if (null == imageCache.getCache(key)) {
                    Log.d("cache", "被清理的缓存:" + key);
                } else {
                    Log.d("cache", "没被清理的缓存:" + key);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void testNet() {

        String[] temUrls = new String[]{
                "http://img1qn.moko.cc/2016-04-14/61e62d71-f752-4c83-aa16-d569c1213bc5.jpg",
                "http://img3.moko.cc/pics/guanggao/79/img3_src_11151313.jpg",
                "http://img3.moko.cc/pics/guanggao/f5/img3_src_11144248.jpg",
                "http://img3.moko.cc/pics/guanggao/de/img3_src_11137178.jpg",
                "http://img3.moko.cc/pics/guanggao/b1/img3_src_11128413.jpg",
                "http://img2.moko.cc/pics/guanggao/c7/img2_src_9764972.jpg",
                "http://img3.moko.cc/pics/guanggao/23/img3_src_11143276.jpg",
                "http://img2.moko.cc/pics/guanggao/3b/img2_src_10905597.jpg",
                "http://img1qn.moko.cc/2016-04-14/fa1e80bb-c552-4c1d-8f11-bb3d6b82780e.jpg"};
        for (int i = 0; i < temUrls.length; i++) {
            String key = temUrls[i];
            imageCache.addCache(key);
            Log.d("cache", "添加缓存:" + key);
        }

        for (int i = 0; i < temUrls.length; i++) {
            String key = temUrls[i];
            if (null == imageCache.getCache(key)) {
                Log.d("cache", "被清理的缓存:" + key);
            } else {
                Log.d("cache", "没被清理的缓存:" + key);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
