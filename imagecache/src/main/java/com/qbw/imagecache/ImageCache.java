package com.qbw.imagecache;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.qbw.encryption.MD5Util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author qbw
 * @createtime 2016/04/14 19:30
 */


public class ImageCache {

    /**
     * 缓存目录的名字
     */
    private String cacheName;

    /**
     * 缓存的大小
     */
    private int cacheSize;

    private DiskLruCache diskLruCache;

    private ImageCache() {
    }

    /**
     * 从网络获取图片保存到缓存
     * @param imageUrl
     * @return
     */
    public boolean addCache(String imageUrl) {
        boolean b = false;
        HttpURLConnection http = null;
        try {
            http = (HttpURLConnection) new URL(imageUrl).openConnection();
            http.setConnectTimeout(5000);
            http.setReadTimeout(5000);
            http.setRequestMethod("GET");
            http.connect();
            if (HttpURLConnection.HTTP_OK == http.getResponseCode()) {
                InputStream inputStream = http.getInputStream();
                b = addCache(imageUrl, inputStream);
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } finally {
            if (null != http) {
                http.disconnect();
            }
        }
        return b;
    }

    /**
     * @param imageUrl 作为缓存的key
     * @param inputStream imageUrl地址对应的文件流
     * @return
     */
    public boolean addCache(String imageUrl, InputStream inputStream) {
        boolean b = false;
        try {
            //这个key要是唯一的，而且这个key 最长120个字符，且只能包括a-z,0-9,下划线以及减号
            //通过'imageUrl'得到MD5值,保证key的合法
            DiskLruCache.Editor editor = diskLruCache.edit(MD5Util.getDigest(imageUrl));
            //index的取值范围[0,valueCount},我们初始化valueCount为1,所以index只能取0值
            OutputStream outputStream = editor.newOutputStream(0);
            byte[] byteBuff = new byte[8 * 1024];
            int readLen;
            while (-1 != (readLen = inputStream.read(byteBuff))) {
                outputStream.write(byteBuff, 0, readLen);
            }
            outputStream.flush();
            editor.commit();
            diskLruCache.flush();
            b = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public InputStream getCache(String imageUrl) {
        try {
            DiskLruCache.Snapshot snapshot = diskLruCache.get(MD5Util.getDigest(imageUrl));
            if (null != snapshot) {
                return snapshot.getInputStream(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeCache(String imageUrl) {
        try {
            diskLruCache.remove(MD5Util.getDigest(imageUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Builder {

        protected ImageCache imageCache;

        public Builder() {
            imageCache = new ImageCache();
        }

        public Builder cacheName(String cacheName) {
            imageCache.cacheName = cacheName;
            return this;
        }

        public Builder cacheSize(int cacheSize) {
            imageCache.cacheSize = cacheSize;
            return this;
        }

        public ImageCache build(Context context) {
            try {
                File cacheDir = getDiskCacheDir(context, imageCache.cacheName);
                if (!cacheDir.exists()) {
                    cacheDir.mkdirs();
                }
                imageCache.diskLruCache = DiskLruCache.open(cacheDir, getAppVersion(context), 1, imageCache.cacheSize);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return imageCache;
        }

        public File getDiskCacheDir(Context context, String uniqueName) {
            String cachePath;
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                    || !Environment.isExternalStorageRemovable()) {
                cachePath = context.getExternalCacheDir().getPath();
            } else {
                cachePath = context.getCacheDir().getPath();
            }
            return new File(cachePath + File.separator + uniqueName);
        }

        public int getAppVersion(Context context) {
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                return info.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return 1;
        }
    }
}
